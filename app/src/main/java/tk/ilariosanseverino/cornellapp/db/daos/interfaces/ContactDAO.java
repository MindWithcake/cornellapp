package tk.ilariosanseverino.cornellapp.db.daos.interfaces;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;

/**
 * Created by Ilario Sanseverino on 17/08/15.
 */
public interface ContactDAO {
	long addContact(Contact contact);

	Contact getContact(int id);

	List<Contact> getContacts();
}
