package tk.ilariosanseverino.cornellapp;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Abstract activity with basic extra funcionalities. Created by Ilario Sanseverino on 20/08/15.
 */
public abstract class CornellActivity extends AppCompatActivity implements CornellCallbacks {
	private final static String TAG_DIALOG = "CornellActivityDialog";

	@Override public int dpToPx(int dp){
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
	}

	@Override public void showDialog(DialogFragment fragment){
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment prev = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG);

		if(prev != null)
			ft.remove(prev);
		ft.addToBackStack(null);

		fragment.show(ft, TAG_DIALOG);
	}

	@Override public void setTitle(CharSequence title){
		ActionBar bar = getSupportActionBar();
		if(bar != null)
			bar.setTitle(title);
	}
}
