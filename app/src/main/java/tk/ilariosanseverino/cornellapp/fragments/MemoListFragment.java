package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.SwipeDeleteHelper;
import tk.ilariosanseverino.cornellapp.adapters.MemoAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.MemoDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.MemoDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;

/**
 * Main fragment for the Memo Activity. Will display a list of existing memos, with a FAB to create
 * a new one.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class MemoListFragment extends CornellFragment implements View.OnClickListener {
	private final static String TAG_MEMOS = "MemoListFragment.TAG_MEMOS";

	private MemoAdapter mMemoAdapter;
	private MemoDAO dao;
	private MemoGetTask task;

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new MemoDAOImpl(activity);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_memo_recycler, container, false);

		RecyclerView recycler = (RecyclerView)root.findViewById(R.id.memo_list);
		root.findViewById(R.id.event_add_fab).setOnClickListener(this);

		recycler.setHasFixedSize(true);
		recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
		recycler.setAdapter(mMemoAdapter);

		SwipeDeleteHelper deleteHelper = new SwipeDeleteHelper(mMemoAdapter);
		ItemTouchHelper touchHelper = new ItemTouchHelper(deleteHelper);
		touchHelper.attachToRecyclerView(recycler);

		return root;
	}

	@Override public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList(TAG_MEMOS, mMemoAdapter.getMemos());
	}

	@Override public void onResume(){
		super.onResume();
		(task = new MemoGetTask()).execute();
	}

	@Override public void onStop(){
		super.onStop();
		if(task != null)
			task.cancel(true);
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mMemoAdapter = new MemoAdapter(cornellCallbacks, dao);

		if(savedInstanceState != null){
			ArrayList<Memo> memos = savedInstanceState.getParcelableArrayList(TAG_MEMOS);
			mMemoAdapter.addItems(memos);
		}
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.memo_title);
	}

	@Override public void onClick(View v){
		cornellCallbacks.showNewFragment(new MemoEditFragment());
	}

	private class MemoGetTask extends AsyncTask<Void, Void, List<Memo>> {

		@Override protected List<Memo> doInBackground(Void... params){
			return dao.getAllMemos();
		}

		@Override protected void onPostExecute(List<Memo> memos){
			mMemoAdapter.replace(memos);
		}
	}

	private class MemoDelTask extends AsyncTask<Memo, Void, Void> {
		@Override protected Void doInBackground(Memo... params){
			for(Memo memo : params)
				dao.deleteMemo(memo.id);
			return null;
		}
	}
}
