package tk.ilariosanseverino.cornellapp.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import java.util.Calendar;
import java.util.Locale;

import tk.ilariosanseverino.cornellapp.R;

import static android.util.TypedValue.*;

/**
 * Widget rendering a date on a bitmap using device font style.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class DateView extends ImageView {
	private final static float MIN_TEXT_SIZE = 6f;

	private final RectF monthRect;
	private final Paint monthPaint;
	private RectF drawMonth = new RectF();

	private final RectF weekRect;
	private final Paint weekPaint;
	private RectF drawWeek = new RectF();

	private final RectF numRect;
	private final Paint numPaint;
	private RectF drawNum = new RectF();

	private String month, week, num;

	public DateView(Context context){
		this(context, null);
	}

	public DateView(Context context, AttributeSet attrs){
		this(context, attrs, 0);
	}

	public DateView(Context context, AttributeSet attrs, int defStyleAttr){
		super(context, attrs, defStyleAttr);

		setImageResource(R.drawable.calendar_blank);
		Resources res = getResources();

		DisplayMetrics metrics = res.getDisplayMetrics();
		monthRect = fromPxCorners(15, 7, 135, 38, metrics);
		weekRect = fromPxCorners(10, 115, 140, 143, metrics);
		numRect = fromPxCorners(30, 50, 120, 110, metrics);

		monthPaint = new Paint(Paint.SUBPIXEL_TEXT_FLAG | Paint.HINTING_ON);
		monthPaint.setTextAlign(Paint.Align.CENTER);
		numPaint = new Paint(monthPaint);
		numPaint.setColor(res.getColor(R.color.black));
		weekPaint = new Paint(numPaint);
		monthPaint.setColor(res.getColor(R.color.white));

		setDate(System.currentTimeMillis());
	}

	private RectF fromPxCorners(int left, int top, int right, int bottom, DisplayMetrics metrics){
		return new RectF(applyDimension(COMPLEX_UNIT_DIP, left, metrics), applyDimension(
				COMPLEX_UNIT_DIP, top, metrics), applyDimension(COMPLEX_UNIT_DIP, right, metrics),
				applyDimension(COMPLEX_UNIT_DIP, bottom, metrics));
	}

	private RectF setTextSize(RectF box, Paint toSet, String toDraw){
		float textSize = MIN_TEXT_SIZE, textHeight, textBottom;
		RectF scaledBox = new RectF();
		getImageMatrix().mapRect(scaledBox, box);
		Rect bounds = new Rect();
		do{
			toSet.setTextSize(++textSize);
			Paint.FontMetrics metrics = toSet.getFontMetrics();
			toSet.getTextBounds(toDraw, 0, toDraw.length(), bounds);
			textBottom = metrics.bottom;
			textHeight = metrics.bottom - metrics.top;
		}
		while(scaledBox.height() > textHeight && scaledBox.width() > bounds.width());

		toSet.setTextSize(--textSize);
		scaledBox.bottom -= textBottom;
		return scaledBox;
	}

	public void setDate(long unixDate){
		Calendar cal = Calendar.getInstance();
		Locale locale = Locale.getDefault();
		cal.setTimeInMillis(unixDate);

		month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
		week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);
		num = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

		drawMonth = setTextSize(monthRect, monthPaint, month);
		drawWeek = setTextSize(weekRect, weekPaint, week);
		drawNum = setTextSize(numRect, numPaint, num);
		invalidate();
	}

	@Override protected void onDraw(@NonNull Canvas canvas){
		super.onDraw(canvas);
		canvas.drawText(month, drawMonth.centerX(), drawMonth.bottom, monthPaint);
		canvas.drawText(week, drawWeek.centerX(), drawWeek.bottom, weekPaint);
		canvas.drawText(num, drawNum.centerX(), drawNum.bottom, numPaint);
	}
}
