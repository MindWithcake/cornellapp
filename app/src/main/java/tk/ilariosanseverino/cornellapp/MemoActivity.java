package tk.ilariosanseverino.cornellapp;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import tk.ilariosanseverino.cornellapp.fragments.MemoListFragment;

/**
 * Activity to add memo and receive notifications.
 * <p/>
 * Created by Ilario Sanseverino on 20/08/15.
 */
public class MemoActivity extends CornellActivity {

	private boolean mDoubleSide;

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memo);

		mDoubleSide = findViewById(R.id.master_container) != null;

		if(savedInstanceState == null)
			setupFragments();
	}

	private void setupFragments(){
		CornellFragment list = new MemoListFragment();
		FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
		tr.add(R.id.slave_container, list);
		if(mDoubleSide){
			//TODO add new memo view on slave
//			tr.add(R.id.master_container, null);
		}
		tr.commit();
	}

	@Override public void showNewFragment(CornellFragment frag){
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.slave_container, frag).addToBackStack(null).commit();
	}
}
