package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.ByCourseAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.OccupationDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.OccupationDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Show Created by Ilario Sanseverino on 17/08/15.
 */
public class TimetableDetailCourseFragment extends CornellFragment {
	private final static String COURSE_ARG = "TimetableDetailCourseFragment.COURSE_ARG";

	protected ByCourseAdapter adapter;
	private Thread queryThread;
	protected OccupationDAO dao;
	private Course course;

	public static CornellFragment makeInstance(Course course){
		Bundle arg = new Bundle();
		arg.putParcelable(COURSE_ARG, course);

		CornellFragment frag = new TimetableDetailCourseFragment();
		frag.setArguments(arg);

		return frag;
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_timetable_detail, container, false);

		GridView list = (GridView)root.findViewById(R.id.time_detail_list);
		list.setAdapter(adapter);
		queryThread = getQueryThread();
		queryThread.start();

		return root;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new OccupationDAOImpl(activity);
		adapter = new ByCourseAdapter(activity);
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		course = getArguments().getParcelable(COURSE_ARG);
	}

	@Override public void onDestroyView(){
		super.onDestroyView();
		queryThread.interrupt();
		queryThread = null;
	}

	protected Thread getQueryThread(){
		return new QueryThread();
	}

	@Override protected CharSequence getTitle(){
		return course.acronym;
	}

	private class QueryThread extends Thread {
		private Handler handler;

		public QueryThread(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			final List<Occupation> occupations = dao.getCourseOccupations(course);
			if(occupations != null && !isInterrupted()){
				handler.post(new Runnable() {
					@Override public void run(){
						adapter.addAll(occupations);
					}
				});
			}
		}
	}
}
