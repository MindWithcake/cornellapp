package tk.ilariosanseverino.cornellapp;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Item for the {@link CornellExpandableAdapter}. Created by Ilario Sanseverino on 16/08/15.
 */
public class CornellExpandableGroup<G, C> {
	public final G groupKey;
	private final List<C> groupChildren;

	public CornellExpandableGroup(@NonNull G groupKey){
		this.groupKey = groupKey;
		this.groupChildren = new ArrayList<>();
	}

	public List<C> getChildren(){
		return new ArrayList<>(groupChildren);
	}

	public void addChild(C child){
		groupChildren.add(child);
	}

	@Override public boolean equals(Object o){
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		CornellExpandableGroup<?, ?> that = (CornellExpandableGroup<?, ?>)o;

		return groupKey.equals(that.groupKey);
	}

	@Override public int hashCode(){
		return groupKey.hashCode();
	}
}
