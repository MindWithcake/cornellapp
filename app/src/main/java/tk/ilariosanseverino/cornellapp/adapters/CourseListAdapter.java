package tk.ilariosanseverino.cornellapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collection;

import tk.ilariosanseverino.cornellapp.CornellExpandableAdapter;
import tk.ilariosanseverino.cornellapp.CornellExpandableGroup;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;

/**
 * Adapter for the expandable list of courses. Created by Ilario Sanseverino on 04/08/15.
 */
public class CourseListAdapter extends CornellExpandableAdapter<String, Course> {

	public void addCourse(Course course, String area){
		CornellExpandableGroup<String, Course> courseGroup = null;
		for(CornellExpandableGroup<String, Course> group : groups){
			if(group.groupKey.equalsIgnoreCase(area)){
				courseGroup = group;
				break;
			}
		}

		if(courseGroup == null){
			courseGroup = new CornellExpandableGroup<>(area);
			groups.add(courseGroup);
		}

		courseGroup.addChild(course);
	}

	public void addCourse(Course course){
		addCourse(course, course.area);
	}

	public void addCourses(Collection<Course> courses){
		groups.clear();
		for(Course course : courses)
			addCourse(course);
	}

	@Override
	public View getGroupView(int position, boolean isExpanded, View convertView, ViewGroup parent){
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.view_course_group, parent, false);
		}
		((TextView)convertView).setText(groups.get(position).groupKey);
		return convertView;
	}

	@Override public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
			View convertView, ViewGroup parent){
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.view_course_child, parent, false);
		}
		Course course = groups.get(groupPosition).getChildren().get(childPosition);
		((TextView)convertView).setText(course.name);
		return convertView;
	}
}
