package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.ContactAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.ContactDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.ContactDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;

/**
 * Fragment used to show the contact list. Created by Ilario Sanseverino on 11/08/2015.
 */
public class ContactsFragment extends CornellFragment {
	private Thread queryThread;
	private ContactAdapter adapter;
	private ContactDAO contactDAO;

	@Override @Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_contacts, container, false);

		GridView contactList = (GridView)root.findViewById(R.id.contact_list);

		adapter = new ContactAdapter(root.getContext(), cornellCallbacks);
		contactList.setAdapter(adapter);

		return root;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		contactDAO = new ContactDAOImpl(activity);
	}

	@Override public void onStart(){
		super.onStart();
		queryThread = new QueryThread();
		queryThread.start();
	}

	@Override public void onStop(){
		super.onStop();
		queryThread.interrupt();
		queryThread = null;
	}

	private class QueryThread extends Thread {
		private Handler handler;

		public QueryThread(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			final List<Contact> contacts = contactDAO.getContacts();
			if(!isInterrupted())
				handler.post(new Runnable() {
					@Override public void run(){
						adapter.addAll(contacts);
					}
				});
		}
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.contact_title);
	}
}
