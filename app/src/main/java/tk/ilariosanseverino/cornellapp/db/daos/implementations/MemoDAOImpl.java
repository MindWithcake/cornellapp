package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.interfaces.MemoDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;

import static tk.ilariosanseverino.cornellapp.db.contracts.MemoEntries.*;

/**
 * Implementation of the Memo DAO using android SQLite connection.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class MemoDAOImpl extends CornellDAO<Memo> implements MemoDAO {
	public MemoDAOImpl(Context ctx){
		super(ctx);
	}

	public static long addMemo(SQLiteDatabase db, Memo memo){
		ContentValues cv = new ContentValues();

		cv.put(NAME_COL, memo.name);
		cv.put(NOTES_COL, memo.notes);
		cv.put(DATE_COL, memo.date);

		if(memo.id < 0)
			return db.insert(TABLE, null, cv);

		db.update(TABLE, cv, _ID + "=?", new String[]{String.valueOf(memo.id)});
		return memo.id;
	}

	@Override public long addMemo(Memo memo){
		try(SQLiteDatabase db = helper.getWritableDatabase()){
			return addMemo(db, memo);
		}
	}

	@Override public Memo getMemo(int id){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryByID(db, TABLE, id)){
			if(!cur.moveToNext())
				return null;
			return makeDAO(cur);
		}
	}

	@Override public List<Memo> getAllMemos(){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryAll(db, TABLE)){
			return makeDAOList(cur);
		}
	}

	@Override public int getID(Memo memo){
		String[] whereCol = new String[]{memo.name};
		String[] select = new String[]{_ID};
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = db.query(TABLE, select, NAME_COL + "=?", whereCol, null, null, null)){
			if(!cur.moveToNext())
				return -1;
			return cur.getInt(cur.getColumnIndex(_ID));
		}
	}

	@Override public void deleteMemo(long id){
		try(SQLiteDatabase db = helper.getWritableDatabase()){
			db.delete(TABLE, _ID + "=?", new String[]{String.valueOf(id)});
		}
	}

	@Override Memo makeDAO(Cursor cur){
		String name = cur.getString(cur.getColumnIndex(NAME_COL));
		String notes = cur.getString(cur.getColumnIndex(NOTES_COL));
		long date = cur.getLong(cur.getColumnIndex(DATE_COL));
		long id = cur.getLong(cur.getColumnIndex(_ID));

		return new Memo(name, notes, date, id);
	}
}
