package tk.ilariosanseverino.cornellapp.db.contracts;

import android.provider.BaseColumns;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Contract for the Contact table. Created by Ilario Sanseverino on 17/08/15.
 */
public abstract class ContactEntries implements BaseColumns {
	private ContactEntries(){}

	public static final String TABLE = "tbContact";

	public static final String NAME_COL = "ConatactName";
	public static final String NAME_TYPE = " TEXT";

	public static final String PHOTO_COL = "ContactPhoto";
	public static final String PHOTO_TYPE = " INTEGER";

	public static final String MAIL_COL = "ContactEmail";
	public static final String MAIL_TYPE = " TEXT";

	public static final String SMS_COL = "ContactSms";
	public static final String SMS_TYPE = " TEXT";

	public static final String CALL_COL = "ContactPhone";
	public static final String CALL_TYPE = " TEXT";

	public static final String CREATION = CornellHelper.creationCommand(TABLE, _ID, NAME_COL,
			NAME_TYPE, PHOTO_COL, PHOTO_TYPE, MAIL_COL, MAIL_TYPE, SMS_COL, SMS_TYPE, CALL_COL,
			CALL_TYPE);
}
