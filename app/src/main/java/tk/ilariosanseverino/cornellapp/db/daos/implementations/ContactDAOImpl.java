package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.interfaces.ContactDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;

import static tk.ilariosanseverino.cornellapp.db.contracts.ContactEntries.*;

/**
 * DAO object to retrieve the Cornell contact from a SQLite database. Created by Ilario Sanseverino
 * on 17/08/15.
 */
public class ContactDAOImpl extends CornellDAO<Contact> implements ContactDAO {
	public ContactDAOImpl(Context ctx){
		super(ctx);
	}

	public static long addContact(SQLiteDatabase db, Contact contact){
		ContentValues cv = new ContentValues();
		cv.put(NAME_COL, contact.name);
		cv.put(PHOTO_COL, contact.photoRes);
		cv.put(MAIL_COL, contact.email);
		cv.put(CALL_COL, contact.phoneNum);
		cv.put(SMS_COL, contact.smsNum);
		return db.insert(TABLE, null, cv);
	}

	@Override public long addContact(Contact contact){
		try(SQLiteDatabase db = helper.getWritableDatabase()){
			return addContact(db, contact);
		}
	}

	@Override public Contact getContact(int id){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryByID(db, TABLE, id)){
			if(!cur.moveToNext())
				return null;
			return makeDAO(cur);
		}
	}

	@Override public List<Contact> getContacts(){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryAll(db, TABLE)){
			return makeDAOList(cur);
		}
	}

	@Override Contact makeDAO(Cursor cursor){
		String name = cursor.getString(cursor.getColumnIndex(NAME_COL));
		int photo = cursor.getInt(cursor.getColumnIndex(PHOTO_COL));
		String email = cursor.getString(cursor.getColumnIndex(MAIL_COL));
		String phone = cursor.getString(cursor.getColumnIndex(CALL_COL));
		String sms = cursor.getString(cursor.getColumnIndex(SMS_COL));
		return new Contact(name, photo, email, phone, sms);
	}
}
