package tk.ilariosanseverino.cornellapp.db.contracts;

import android.provider.BaseColumns;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Contract for the Campus table. Created by Ilario Sanseverino on 17/08/15.
 */
public abstract class CampusEntries implements BaseColumns {
	public static final String TABLE = "tbCampus";

	public static final String NAME_COL = "CampusName";
	public static final String NAME_TYPE = " TEXT";

	public static final String DESCRIBE_COL = "CampusDescription";
	public static final String DESCRIBE_TYPE = " TEXT";

	public static final String LAT_COL = "CampusLatitude";
	public static final String LAT_TYPE = " REAL";

	public static final String LONG_COL = "CampusLongitude";
	public static final String LONG_TYPE = " REAL";

	public static final String CREATION = CornellHelper.creationCommand(TABLE, _ID, NAME_COL,
			NAME_TYPE, DESCRIBE_COL, DESCRIBE_TYPE, LAT_COL, LAT_TYPE, LONG_COL, LONG_TYPE);

	private CampusEntries(){}
}
