package tk.ilariosanseverino.cornellapp;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;

/**
 * Created by Ilario Sanseverino on 22/08/15.
 */
public class SwipeDeleteHelper extends SimpleCallback implements OnClickListener {
	private int deleteIndex;
	private OnDeleteListener mDeleteListener;

	public SwipeDeleteHelper(OnDeleteListener listener){
		super(0, ItemTouchHelper.END);
		mDeleteListener = listener;
	}

	@Override public void onClick(DialogInterface dialog, int which){
		if(mDeleteListener == null)
			return;

		if(which == DialogInterface.BUTTON_POSITIVE)
			mDeleteListener.onItemDeleted(deleteIndex);
		else
			mDeleteListener.onCancelDelete(deleteIndex);

		dialog.dismiss();
	}

	@Override public boolean onMove(RecyclerView view, ViewHolder holder, ViewHolder target){
		return false;
	}

	@Override public void onSwiped(ViewHolder holder, int direction){
		deleteIndex = holder.getAdapterPosition();
		new AlertDialog.Builder(holder.itemView.getContext()).setMessage(
				R.string.event_delete_confirm).setPositiveButton(R.string.event_delete_yes, this)
															 .setNegativeButton(
																	 R.string.event_delete_no,
																	 this)
															 .show();
	}

	public interface OnDeleteListener {
		void onItemDeleted(int adapterPosition);

		void onCancelDelete(int adapterPosition);
	}
}
