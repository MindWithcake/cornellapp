package tk.ilariosanseverino.cornellapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import tk.ilariosanseverino.cornellapp.CornellCallbacks;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;
import tk.ilariosanseverino.cornellapp.fragments.SmsDialogFragment;
import tk.ilariosanseverino.cornellapp.ui.SquareTextView;

/**
 * Adapter for the contact list. Created by Ilario Sanseverino on 11/08/2015.
 */
public class ContactAdapter extends ArrayAdapter<Contact> {
	private final LayoutInflater inflater;
	private final CornellCallbacks activityCallbacks;

	public ContactAdapter(Context context, CornellCallbacks callbacks){
		super(context, 0);
		inflater = LayoutInflater.from(context);
		activityCallbacks = callbacks;
	}

	@Override public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder;
		if(convertView == null){
			View internal = inflater.inflate(R.layout.view_contact_item, parent, false);
			holder = new ViewHolder();
			holder.photoView = (SquareTextView)internal.findViewById(R.id.contact_photo);
			holder.callButton = (ImageButton)internal.findViewById(R.id.contact_call_button);
			holder.smsButton = (ImageButton)internal.findViewById(R.id.contact_sms_button);
			holder.mailButton = (ImageButton)internal.findViewById(R.id.contact_mail_button);
			holder.callNumberView = (TextView)internal.findViewById(R.id.contact_phone_number);
			holder.smsNumberView = (TextView)internal.findViewById(R.id.contact_sms_number);
			holder.mailAddressView = (TextView)internal.findViewById(R.id.contact_mail_address);

			Context ctx = parent.getContext();
			convertView = new CardView(ctx);
			((CardView)convertView).setCardElevation(0);
			((CardView)convertView).addView(internal);
			int bg = ctx.getResources().getColor(R.color.orange_alpha);
			((CardView)convertView).setCardBackgroundColor(bg);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder)convertView.getTag();

		final Contact item = getItem(position);
		holder.photoView.setText(item.name);
		holder.photoView.setCompoundDrawablesWithIntrinsicBounds(0, item.photoRes, 0, 0);
		holder.smsNumberView.setText(item.smsNum);
		holder.callNumberView.setText(item.phoneNum);
		holder.mailAddressView.setText(item.email);

		holder.smsButton.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){
				activityCallbacks.showDialog(SmsDialogFragment.makeInstance(item.smsNum));
			}
		});

		holder.mailButton.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){
				Uri mailUri = Uri.fromParts("mailto", item.email, null);
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, mailUri);
				getContext().startActivity(Intent.createChooser(emailIntent, "Send email\u2026"));
			}
		});

		holder.callButton.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){
				Uri callUri = Uri.fromParts("tel", item.phoneNum, null);
				Intent callIntent = new Intent(Intent.ACTION_CALL, callUri);
				getContext().startActivity(Intent.createChooser(callIntent, "Call"));
			}
		});

		return convertView;
	}

	static class ViewHolder {
		private SquareTextView photoView;

		private ImageButton callButton;
		private ImageButton smsButton;
		private ImageButton mailButton;

		private TextView callNumberView;
		private TextView smsNumberView;
		private TextView mailAddressView;
	}
}
