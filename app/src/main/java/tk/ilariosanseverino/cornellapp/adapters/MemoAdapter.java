package tk.ilariosanseverino.cornellapp.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;

import tk.ilariosanseverino.cornellapp.CornellCallbacks;
import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.MemoHolder;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.SwipeDeleteHelper.OnDeleteListener;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.MemoDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;
import tk.ilariosanseverino.cornellapp.fragments.MemoEditFragment;

/**
 * Adapter for the Memo list in {@link tk.ilariosanseverino.cornellapp.fragments.MemoListFragment}.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class MemoAdapter extends RecyclerView.Adapter<MemoHolder> implements OnDeleteListener {
	private final ArrayList<Memo> mMemos;
	private final CornellCallbacks mCallbacks;
	private final MemoDAO dao;

	public MemoAdapter(CornellCallbacks callbacks, MemoDAO dao){
		this.dao = dao;
		this.mMemos = new ArrayList<>();
		this.mCallbacks = callbacks;
	}

	public void replace(Collection<Memo> memos){
		if(memos != null){
			mMemos.clear();
			addItems(memos);
		}
	}

	public void addItems(Collection<Memo> memos){
		if(memos != null && mMemos.addAll(memos))
			notifyDataSetChanged();
	}

	@Override public MemoHolder onCreateViewHolder(ViewGroup parent, int i){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View v = inflater.inflate(R.layout.view_memo_list_item, parent, false);
		return new MemoHolder(v, new MemoHolder.OnMemoCardClick() {
			@Override public void onMemoCardClick(Memo memo){
				CornellFragment frag = MemoEditFragment.makeInstance(memo);
				mCallbacks.showNewFragment(frag);
			}
		});
	}

	@Override public void onBindViewHolder(MemoHolder memoHolder, int i){
		memoHolder.bindMemo(mMemos.get(i));
	}

	@Override public int getItemCount(){
		return mMemos.size();
	}

	public ArrayList<Memo> getMemos(){
		return new ArrayList<>(mMemos);
	}

	@Override public void onItemDeleted(final int adapterPosition){
		final Handler handler = new Handler(Looper.getMainLooper());
		final Memo memo = mMemos.remove(adapterPosition);
		new Thread() {
			@Override public void run(){
				dao.deleteMemo(memo.id);
				handler.post(new Runnable() {
					@Override public void run(){
						notifyItemRemoved(adapterPosition);
					}
				});
			}
		}.start();
	}

	@Override public void onCancelDelete(int adapterPosition){
		notifyItemChanged(adapterPosition);
	}
}
