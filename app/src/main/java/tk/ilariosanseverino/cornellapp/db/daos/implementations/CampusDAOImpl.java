package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CampusDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;

import static tk.ilariosanseverino.cornellapp.db.contracts.CampusEntries.*;

/**
 * DAO to access the table with information about Cornell campuses. Created by Ilario Sanseverino on
 * 16/08/15.
 */
public class CampusDAOImpl extends CornellDAO<Campus> implements CampusDAO {
	public CampusDAOImpl(Context ctx){
		super(ctx);
	}

	public static long addCampus(SQLiteDatabase db, Campus campus){
		ContentValues cv = new ContentValues();

		cv.put(NAME_COL, campus.name);
		cv.put(DESCRIBE_COL, campus.description);
		cv.put(LAT_COL, campus.latitude);
		cv.put(LONG_COL, campus.longitude);

		return db.insert(TABLE, null, cv);
	}

	@Override public long addCampus(Campus campus){
		try(SQLiteDatabase db = helper.getWritableDatabase()){
			return addCampus(db, campus);
		}
	}

	@Override public Campus getCampus(int id){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryByID(db, TABLE, id)){
			if(!cur.moveToNext())
				return null;
			return makeDAO(cur);
		}
	}

	@Override public List<Campus> getAllCampus(){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryAll(db, TABLE)){
			return makeDAOList(cur);
		}
	}

	@Override public int getID(String campusName){
		String[] whereCol = new String[]{campusName};
		String[] select = new String[]{_ID};
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = db.query(TABLE, select, NAME_COL + "=?", whereCol, null, null, null)){
			if(!cur.moveToNext())
				return -1;
			return cur.getInt(cur.getColumnIndex(_ID));
		}
	}

	@Override Campus makeDAO(Cursor cursor){
		String name = cursor.getString(cursor.getColumnIndex(NAME_COL));
		String description = cursor.getString(cursor.getColumnIndex(DESCRIBE_COL));
		double lat = cursor.getDouble(cursor.getColumnIndex(LAT_COL));
		double lon = cursor.getDouble(cursor.getColumnIndex(LONG_COL));

		return new Campus(name, description, lat, lon);
	}
}
