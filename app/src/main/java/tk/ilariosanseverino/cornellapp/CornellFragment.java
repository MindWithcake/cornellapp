package tk.ilariosanseverino.cornellapp;

import android.app.Activity;
import android.support.v4.app.Fragment;

/**
 * Abstract fragment to handle the attach/detach of the callback. Created by Ilario Sanseverino on
 * 10/08/2015.
 */
public abstract class CornellFragment extends Fragment {
	protected CornellCallbacks cornellCallbacks;

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		if(!(activity instanceof CornellCallbacks))
			throw new IllegalArgumentException("Activity must implement CornellCallbacks!");
		cornellCallbacks = (CornellCallbacks)activity;
	}

	@Override public void onDetach(){
		super.onDetach();
		cornellCallbacks = null;
	}

	@Override public void onResume(){
		super.onResume();
		cornellCallbacks.setTitle(getTitle());
	}

	/**
	 * Give a title to be displayed in the ActionBar. This default implementation returns the
	 * application name.
	 *
	 * @return the desired title.
	 */
	protected CharSequence getTitle(){
		return getString(R.string.app_name);
	}
}
