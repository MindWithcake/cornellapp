package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Abstract object providing DAO implementation setup. Created by Ilario Sanseverino on 16/08/15.
 */
abstract class CornellDAO<T> {
	protected final CornellHelper helper;

	protected CornellDAO(Context ctx){
		helper = CornellHelper.getInstance(ctx);
	}

	private static String[] idCol(int id){
		return new String[]{String.valueOf(id)};
	}

	protected static Cursor queryByID(SQLiteDatabase db, String table, int id){
		return db.query(table, null, BaseColumns._ID + "=?", idCol(id), null, null, null);
	}

	protected static Cursor queryAll(SQLiteDatabase db, String table){
		return db.query(table, null, null, null, null, null, null);
	}

	protected List<T> makeDAOList(Cursor cur){
		if(!cur.moveToFirst())
			return null;

		List<T> list = new ArrayList<>();
		do
			list.add(makeDAO(cur));
		while(cur.moveToNext());

		return list;
	}

	abstract T makeDAO(Cursor cur);
}
