package tk.ilariosanseverino.cornellapp.fragments.tabs;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.adapters.TimetableListAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;
import tk.ilariosanseverino.cornellapp.fragments.TimetableDetailRoomFragment;

/**
 * Let the user choose a room of which to view the occupation time. Created by Ilario Sanseverino on
 * 15/08/15.
 */
public class RoomTimetableFragment extends TabListFragment {
	public static Fragment makeInstance(ArrayList<Occupation> times){
		TabListFragment frag = new RoomTimetableFragment();
		frag.addArgument(times);
		return frag;
	}

	@Override protected TimetableListAdapter getAdapter(List<Occupation> times){
		TimetableListAdapter adapter = new TimetableListAdapter();
		for(Occupation o : times)
			adapter.addChild(o.campus, o.room);
		return adapter;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int group, int child, long id){
		String room = adapter.getChild(group, child).toString();
		new QueryThread(room).start();
		return true;
	}

	private class QueryThread extends Thread {
		private final String room;
		private final Handler handler;

		public QueryThread(String room){
			this.room = room;
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			handler.post(new Runnable() {
				@Override public void run(){
					CornellFragment frag = TimetableDetailRoomFragment.makeInstance(room);
					cornellCallbacks.showNewFragment(frag);
				}
			});
		}
	}
}
