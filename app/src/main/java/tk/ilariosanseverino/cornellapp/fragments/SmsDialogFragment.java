package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import tk.ilariosanseverino.cornellapp.R;

/**
 * Dialog to send an SMS. Created by Ilario Sanseverino on 19/08/15.
 */
public class SmsDialogFragment extends DialogFragment implements View.OnClickListener {
	private final static String NUM_ARG = "SmsDialogFragment.NUM_ARG";

	private EditText messageView;
	private String number;

	public static DialogFragment makeInstance(String number){
		Bundle arg = new Bundle();
		arg.putString(NUM_ARG, number);

		DialogFragment frag = new SmsDialogFragment();
		frag.setArguments(arg);

		return frag;
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		number = getArguments().getString(NUM_ARG);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_dialog_sms, container, false);

		((EditText)root.findViewById(R.id.num_field)).setText(number);
		root.findViewById(R.id.send_button).setOnClickListener(this);
		messageView = (EditText)root.findViewById(R.id.message_body);

		return root;
	}

	@Override public void onClick(View v){
		String sms = messageView.getText().toString();
		SmsManager.getDefault().sendTextMessage(number, null, sms, null, null);
		Toast t = Toast.makeText(getActivity(), "Message sent successfully", Toast.LENGTH_SHORT);
		t.setGravity(Gravity.CENTER, 0, 0);
		t.show();
		dismiss();
	}

	@NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState){
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setTitle(R.string.sms_dialog_title);
		((TextView)dialog.findViewById(android.R.id.title)).setGravity(Gravity.CENTER);
		return dialog;
	}
}
