package tk.ilariosanseverino.cornellapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;
import tk.ilariosanseverino.cornellapp.fragments.tabs.CourseTimetableFragment;
import tk.ilariosanseverino.cornellapp.fragments.tabs.DateTimetableFragment;
import tk.ilariosanseverino.cornellapp.fragments.tabs.RoomTimetableFragment;

/**
 * Adapter for the ViewPager in the Timetable selection screen. It offer 3 options: Show weekly
 * timetables for a single course, Show weekly occupation for the given room, Show all courses for
 * the given date and time. Created by Ilario Sanseverino on 15/08/15.
 */
public class TimetablePagerAdapter extends FragmentPagerAdapter {
	private final static String[] TITLES = {"Course", "Room", "Date"};

	private final ArrayList<Occupation> times;

	public TimetablePagerAdapter(FragmentManager fm, ArrayList<Occupation> times){
		super(fm);
		this.times = times;
	}

	@Override public Fragment getItem(int position){
		switch(position){
		case 0:
			return CourseTimetableFragment.makeInstance(times);
		case 1:
			return RoomTimetableFragment.makeInstance(times);
		case 2:
			return new DateTimetableFragment();
		default:
			throw new IndexOutOfBoundsException("Index " + position + " shoul be in [0-2]");
		}
	}

	@Override public int getCount(){
		return 3;
	}

	@Override public CharSequence getPageTitle(int position){
		return TITLES[position];
	}
}
