package tk.ilariosanseverino.cornellapp.fragments.tabs;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.TimetableListAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.CourseDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CourseDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;
import tk.ilariosanseverino.cornellapp.fragments.TimetableDetailCourseFragment;

/**
 * Tab fragment to show timetables relative to a single course. Created by Ilario Sanseverino on
 * 15/08/15.
 */
public class CourseTimetableFragment extends TabListFragment {
	private CourseDAO dao;

	public static Fragment makeInstance(ArrayList<Occupation> times){
		CourseTimetableFragment frag = new CourseTimetableFragment();
		frag.addArgument(times);
		return frag;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new CourseDAOImpl(activity);
	}

	@Override protected TimetableListAdapter getAdapter(List<Occupation> times){
		TimetableListAdapter adapter = new TimetableListAdapter();
		for(Occupation o : times)
			adapter.addChild(o.courseArea, o.courseAcronym);
		return adapter;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int group, int child, long id){
		String acronym = adapter.getChild(group, child).toString();
		new QueryThread(acronym).start();
		return true;
	}

	private class QueryThread extends Thread {
		private final String acronym;
		private final Handler handler;

		public QueryThread(String acronym){
			this.acronym = acronym;
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			final Course selected = dao.getCourse(dao.getID(acronym));
			handler.post(new Runnable() {
				@Override public void run(){
					CornellFragment frag = TimetableDetailCourseFragment.makeInstance(selected);
					cornellCallbacks.showNewFragment(frag);
				}
			});
		}
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.time_title);
	}
}
