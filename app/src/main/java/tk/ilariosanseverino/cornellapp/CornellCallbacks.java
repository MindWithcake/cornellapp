package tk.ilariosanseverino.cornellapp;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * Communication interface between fragments and activity. Created by Ilario Sanseverino on
 * 10/08/2015.
 */
public interface CornellCallbacks {
	/**
	 * Shows a {@link Fragment} in the main screen. On tablets it's the left side, while on
	 * handheld
	 * devices is the whole screen.
	 *
	 * @param frag the Fragment to show.
	 */
	void showNewFragment(CornellFragment frag);

	/**
	 * Convert a size in dp to a size in pixel.
	 *
	 * @param dp the size to convert.
	 * @return the size in px for the given dps on the current device.
	 */
	int dpToPx(int dp);

	/**
	 * Show a Dialog Fragment.
	 *
	 * @param fragment the dialog to show.
	 */
	void showDialog(DialogFragment fragment);

	/**
	 * Sets the ActionBar title (if possible).
	 *
	 * @param title the title to show.
	 */
	void setTitle(CharSequence title);
}
