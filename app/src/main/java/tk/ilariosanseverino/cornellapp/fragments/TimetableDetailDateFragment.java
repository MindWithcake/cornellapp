package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.adapters.ByDateAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Fragment to show timetables for a single day. Created by Ilario Sanseverino on 19/08/15.
 */
public class TimetableDetailDateFragment extends TimetableDetailCourseFragment {
	private final static String DAY_ARG = "TimetableDetailDateFragment.DAY_ARG";

	private Occupation.WeekDay day;

	public static CornellFragment makeInstance(Occupation.WeekDay day){
		Bundle arg = new Bundle();
		arg.putString(DAY_ARG, day.name());

		CornellFragment frag = new TimetableDetailDateFragment();
		frag.setArguments(arg);

		return frag;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		adapter = new ByDateAdapter(activity);
	}

	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		day = Occupation.WeekDay.valueOf(getArguments().getString(DAY_ARG));
	}

	@Override protected Thread getQueryThread(){
		return new QueryThread();
	}

	@Override protected CharSequence getTitle(){
		return day.name();
	}

	private class QueryThread extends Thread {
		@Override public void run(){
			final List<Occupation> occupations = dao.getDailyOccupations(day);
			if(occupations != null && !Thread.interrupted()){
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override public void run(){
						adapter.addAll(occupations);
					}
				});
			}
		}
	}
}
