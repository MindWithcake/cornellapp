package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.MemoDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.MemoDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;
import tk.ilariosanseverino.cornellapp.ui.DateView;

/**
 * Create or edit a memo. Created by Ilario Sanseverino on 21/08/15.
 */
public class MemoEditFragment extends CornellFragment {
	private final static String TAG_MEMO = "MemoEditFragment.TAG_MEMO";
	private final static int REQUEST_DATE = 35145;

	private EditText nameField;
	private EditText notesField;
	private DateView dateView;

	private long memoId = -1, date = System.currentTimeMillis();
	private MemoDAO dao;

	public static CornellFragment makeInstance(Memo memo){
		Bundle arg = new Bundle();
		arg.putParcelable(TAG_MEMO, memo);

		CornellFragment frag = new MemoEditFragment();
		frag.setArguments(arg);
		return frag;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new MemoDAOImpl(activity);
	}

	@Override public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		Bundle b = getArguments();
		if(b == null){
			b = new Bundle();
			setArguments(b);
		}
		b.putParcelable(TAG_MEMO, makeMemo());
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_memo_edit, container, false);

		nameField = (EditText)root.findViewById(R.id.event_name_field);
		notesField = (EditText)root.findViewById(R.id.event_notes_field);
		dateView = (DateView)root.findViewById(R.id.date_picker_icon);
		root.findViewById(R.id.event_save_button).setOnClickListener(new SaveClickListener());

		dateView.setOnClickListener(new DateClickListener());

		if(getArguments() != null){
			Memo memo = getArguments().getParcelable(TAG_MEMO);
			if(memo != null){
				nameField.setText(memo.name);
				notesField.setText(memo.notes);
				dateView.setDate(memo.date);
				memoId = memo.id;
			}
		}

		return root;
	}

	@Override public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode == REQUEST_DATE && resultCode == Activity.RESULT_OK){
			date = data.getLongExtra(DateTimePickerDialog.RESULT_DATE, System.currentTimeMillis());
			dateView.setDate(date);
		}
	}

	private Memo makeMemo(){
		String eventName = nameField.getText().toString();
		String eventNotes = notesField.getText().toString();
		return new Memo(eventName, eventNotes, date, memoId);
	}

	private class DateClickListener implements View.OnClickListener {
		@Override public void onClick(View v){
			DialogFragment dialog = new DateTimePickerDialog();
			dialog.setTargetFragment(MemoEditFragment.this, REQUEST_DATE);
			cornellCallbacks.showDialog(dialog);
		}
	}

	private class SaveClickListener implements View.OnClickListener, Runnable {
		private Memo toSave;

		@Override public void onClick(View v){
			Toast t;
			if(!nameField.getText().toString().isEmpty()){
				toSave = makeMemo();
				new Thread(this).start();
				t = Toast.makeText(v.getContext(), R.string.event_ok_toast, Toast.LENGTH_LONG);
			}
			else{
				toSave = null;
				t = Toast.makeText(v.getContext(), R.string.event_error_toast, Toast.LENGTH_SHORT);
			}

			t.setGravity(Gravity.CENTER, 0, 0);
			t.show();
		}

		@Override public void run(){
			if(toSave != null)
				dao.addMemo(toSave);
		}
	}
}
