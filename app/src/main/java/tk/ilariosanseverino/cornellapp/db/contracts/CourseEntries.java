package tk.ilariosanseverino.cornellapp.db.contracts;

import android.provider.BaseColumns;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Contract for the Course table. Created by Ilario Sanseverino on 17/08/15.
 */
public abstract class CourseEntries implements BaseColumns {
	public static final String TABLE = "tbCourse";

	public static final String AREA_COL = "CourseArea";
	public static final String AREA_TYPE = " INTEGER";

	public static final String NAME_COL = "CourseName";
	public static final String NAME_TYPE = " TEXT";

	public static final String GRADE_COL = "CourseGrade";
	public static final String GRADE_TYPE = " INTEGER";

	public static final String ACRONYM_COL = "CourseAcronym";
	public static final String ACRONYM_TYPE = " TEXT";

	public static final String DESCRIPTION_COL = "CourseDescription";
	public static final String DESCRIPTION_TYPE = " TEXT";

	public static final String CREATION = CornellHelper.creationCommand(TABLE, _ID, AREA_COL,
			AREA_TYPE, NAME_COL, NAME_TYPE, ACRONYM_COL, ACRONYM_TYPE, GRADE_COL, GRADE_TYPE,
			DESCRIPTION_COL, DESCRIPTION_TYPE);

	private CourseEntries(){}
}
