package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import java.util.Calendar;

import tk.ilariosanseverino.cornellapp.R;

import static java.util.Calendar.*;

/**
 * Picker for both Date and Time in one place.
 * <p/>
 * Created by Ilario Sanseverino on 22/08/15.
 */
public class DateTimePickerDialog extends DialogFragment
		implements OnDateChangedListener, OnTimeChangedListener, OnClickListener {
	public final static String RESULT_DATE = "DateTimePickerDialog.RESULT_DATE";

	private final Calendar calendar = Calendar.getInstance();
	private boolean cancelled = true;

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_dialog_date_time_picker, container, false);

		DatePicker datePicker = (DatePicker)root.findViewById(R.id.date_picker);
		TimePicker timePicker = (TimePicker)root.findViewById(R.id.time_picker);
		root.findViewById(R.id.positive_button).setOnClickListener(this);
		root.findViewById(R.id.negative_button).setOnClickListener(this);

		datePicker.init(calendar.get(YEAR), calendar.get(MONTH), calendar.get(DAY_OF_MONTH), this);
		timePicker.setOnTimeChangedListener(this);

		return root;
	}

	@Override public void onDateChanged(DatePicker view, int year, int month, int day){
		calendar.set(year, month, day);
	}

	@Override public void onTimeChanged(TimePicker view, int hourOfDay, int minute){
		calendar.set(HOUR_OF_DAY, hourOfDay);
		calendar.set(MINUTE, minute);
	}

	@NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState){
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setTitle(R.string.date_picker_dialog_title);
		return dialog;
	}

	@Override public void onClick(View v){
		cancelled = (v.getId() != R.id.positive_button);
		dismiss();
	}

	@Override public void onDismiss(DialogInterface dialog){
		super.onDismiss(dialog);
		if(!cancelled){
			Fragment target = getTargetFragment();
			if(target != null){
				Intent result = new Intent();
				result.putExtra(RESULT_DATE, calendar.getTimeInMillis());
				target.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);
			}
		}
	}
}
