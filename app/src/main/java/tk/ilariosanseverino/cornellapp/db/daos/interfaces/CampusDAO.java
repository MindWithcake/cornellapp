package tk.ilariosanseverino.cornellapp.db.daos.interfaces;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;

/**
 * Created by Ilario Sanseverino on 16/08/15.
 */
public interface CampusDAO {
	long addCampus(Campus campus);

	Campus getCampus(int id);

	List<Campus> getAllCampus();

	int getID(String campusName);
}
