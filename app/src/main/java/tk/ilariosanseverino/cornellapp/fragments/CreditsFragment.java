package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import tk.ilariosanseverino.cornellapp.R;

/**
 * Dialog shown when action bar "Credits" item is clicked. Created by Ilario Sanseverino on
 * 19/08/15.
 */
public class CreditsFragment extends DialogFragment {
	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_dialog_credit, container, false);

		WebView course = (WebView)root.findViewById(R.id.courses_credits);
		WebView time = (WebView)root.findViewById(R.id.time_credits);
		WebView map = (WebView)root.findViewById(R.id.map_credits);

		course.loadData(getText(R.string.courses_icon_attribution).toString(), "text/html",
				"UTF8");
		time.loadData(getText(R.string.time_icon_attribution).toString(), "text/html", "UTF8");
		map.loadData(getText(R.string.location_icon_attribution).toString(), "text/html", "UTF8");

		return root;
	}

	@NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState){
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setTitle("Credits");
		return dialog;
	}
}
