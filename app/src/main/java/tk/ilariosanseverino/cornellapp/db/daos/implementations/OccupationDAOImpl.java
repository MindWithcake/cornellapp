package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.contracts.CampusEntries;
import tk.ilariosanseverino.cornellapp.db.contracts.CourseEntries;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CampusDAO;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CourseDAO;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.OccupationDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation.WeekDay;

import static tk.ilariosanseverino.cornellapp.db.contracts.CourseEntries.*;
import static tk.ilariosanseverino.cornellapp.db.contracts.OccupationEntries.*;
import static tk.ilariosanseverino.cornellapp.db.contracts.OccupationEntries.TABLE;

/**
 * DAO to access the table with the Occupation times for classrooms. Created by Ilario Sanseverino
 * on 16/08/15.
 */
public class OccupationDAOImpl extends CornellDAO<Occupation> implements OccupationDAO {
	private final static String LOG_TAG = "OccupationDAO";

	private final CampusDAO campusDAO;
	private final CourseDAO courseDAO;

	public OccupationDAOImpl(Context ctx){
		super(ctx);
		campusDAO = new CampusDAOImpl(ctx);
		courseDAO = new CourseDAOImpl(ctx);
	}

	public static long addOccupation(SQLiteDatabase db, Occupation occupation){
		int courseID, campusID;
		try(Cursor cur = db.rawQuery("SELECT " + CourseEntries._ID + " FROM " + CourseEntries.TABLE
				+ " WHERE " + ACRONYM_COL + "=?", new String[]{occupation.courseAcronym})){
			cur.moveToNext();
			courseID = cur.getInt(0);
		}
		try(Cursor cur = db.rawQuery("SELECT " + CampusEntries._ID + " FROM " + CampusEntries.TABLE
				+ " WHERE " + CampusEntries.NAME_COL + "=?", new String[]{occupation.campus})){
			cur.moveToNext();
			campusID = cur.getInt(0);
		}
		return addOccupation(db, occupation, courseID, campusID);
	}

	private static long addOccupation(SQLiteDatabase db, Occupation oc, int courseID, int campusID){
		ContentValues cv = new ContentValues();
		cv.put(COURSE_COL, courseID);
		cv.put(CAMPUS_COL, campusID);
		cv.put(ROOM_COL, oc.room);
		cv.put(DAY_COL, oc.weekDay.name());
		cv.put(START_COL, oc.startTime);
		cv.put(END_COL, oc.endTime);
		return db.insert(TABLE, null, cv);
	}

	@Override synchronized public long addOccupation(Occupation occupation){
		int courseID = courseDAO.getID(occupation.courseAcronym);
		int campusID = campusDAO.getID(occupation.campus);

		try(SQLiteDatabase db = helper.getWritableDatabase()){
			return addOccupation(db, occupation, courseID, campusID);
		}
	}

	@Override synchronized public Occupation getOccupation(int id){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryByID(db, TABLE, id)){
			if(!cur.moveToNext())
				return null;
			return makeDAO(cur);
		}
	}

	@Override synchronized public List<Occupation> getOccupations(){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryAll(db, TABLE)){
			return makeDAOList(cur);
		}
	}

	@Override synchronized public List<Occupation> getCourseOccupations(Course course){
		return getSelectedOccupations(COURSE_COL, String.valueOf(courseDAO.getID(course.acronym)));
	}

	@Override public List<Occupation> getRoomOccupations(String room){
		return getSelectedOccupations(ROOM_COL, room);
	}

	@Override public List<Occupation> getDailyOccupations(WeekDay day){
		return getSelectedOccupations(DAY_COL, day.name());
	}

	@Override Occupation makeDAO(Cursor cur){
		int foreignID = cur.getInt(cur.getColumnIndex(COURSE_COL));
		Course course = courseDAO.getCourse(foreignID);
		if(course == null){
			Log.e(LOG_TAG, "Course is null, should not be. id: " + foreignID);
			return null;
		}

		foreignID = cur.getInt(cur.getColumnIndex(CAMPUS_COL));
		Campus campus = campusDAO.getCampus(foreignID);
		if(campus == null){
			Log.e(LOG_TAG, "Campus is null, should not be. id: " + foreignID);
			return null;
		}

		String room = cur.getString(cur.getColumnIndex(ROOM_COL));
		WeekDay day = WeekDay.valueOf(cur.getString(cur.getColumnIndex(DAY_COL)));
		int start = cur.getInt(cur.getColumnIndex(START_COL));
		int end = cur.getInt(cur.getColumnIndex(END_COL));

		return new Occupation(campus, course, room, day, start, end);
	}

	synchronized private List<Occupation> getSelectedOccupations(String col, String val){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = db.query(TABLE, null, col+"=?", new String[]{val}, null, null, null)){
			return makeDAOList(cur);
		}
	}
}
