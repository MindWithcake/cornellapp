package tk.ilariosanseverino.cornellapp.db.contracts;

import android.provider.BaseColumns;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Contract class for the Memo table.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public abstract class MemoEntries implements BaseColumns {
	public static final String TABLE = "tbMemo";

	public static final String NAME_COL = "MemoName";
	public static final String NAME_TYPE = " TEXT";

	public static final String DATE_COL = "MemoDate";
	public static final String DATE_TYPE = " INTEGER";

	public static final String NOTES_COL = "MemoNotes";
	public static final String NOTES_TYPE = " TEXT";

	public static final String CREATION = CornellHelper.creationCommand(TABLE, _ID, NAME_COL,
			NAME_TYPE, DATE_COL, DATE_TYPE, NOTES_COL, NOTES_TYPE);

	private MemoEntries(){}
}
