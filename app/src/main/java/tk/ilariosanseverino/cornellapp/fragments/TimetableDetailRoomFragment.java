package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.adapters.ByRoomAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Show timetables by room. Some tinkering is required to make it better (Abstract parent + course
 * and room extending it for the common behaviour). Created by Ilario Sanseverino on 19/08/15.
 */
public class TimetableDetailRoomFragment extends TimetableDetailCourseFragment {
	private final static String ROOM_ARG = "TimetableDetailRoomFragment.ROOM_ARG";

	private String room;

	public static CornellFragment makeInstance(String room){
		Bundle arg = new Bundle();
		arg.putString(ROOM_ARG, room);

		CornellFragment frag = new TimetableDetailRoomFragment();
		frag.setArguments(arg);

		return frag;
	}

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		adapter = new ByRoomAdapter(activity);
	}

	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		room = getArguments().getString(ROOM_ARG);
	}

	@Override protected Thread getQueryThread(){
		return new Thread(new QueryRunnable());
	}

	@Override protected CharSequence getTitle(){
		return room;
	}

	private class QueryRunnable implements Runnable {
		private Handler handler;

		public QueryRunnable(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			final List<Occupation> occupations = dao.getRoomOccupations(room);
			if(occupations != null && !Thread.interrupted()){
				handler.post(new Runnable() {
					@Override public void run(){
						adapter.addAll(occupations);
					}
				});
			}
		}
	}
}
