package tk.ilariosanseverino.cornellapp.adapters;

import android.content.Context;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Created by Ilario Sanseverino on 19/08/15.
 */
public class ByRoomAdapter extends ByCourseAdapter {
	public ByRoomAdapter(Context context){
		super(context);
	}

	@Override protected void setContent(Occupation occupation, ViewHolder holder){
		holder.day.setText(occupation.weekDay.name());
		holder.room.setText(occupation.courseAcronym);
		holder.time.setText(timeString(occupation.startTime, occupation.endTime));
	}
}
