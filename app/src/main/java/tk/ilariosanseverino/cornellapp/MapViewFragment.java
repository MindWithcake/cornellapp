package tk.ilariosanseverino.cornellapp;

import android.os.Bundle;

import com.google.android.gms.maps.MapView;

/**
 * Fragment containing a MapView. Provides the lifecycle cascading to the view. Created by Ilario
 * Sanseverino on 19/08/15.
 */
public abstract class MapViewFragment extends CornellFragment {
	protected MapView mapView;

	@Override public void onResume(){
		super.onResume();
		mapView.onResume();
	}

	@Override public void onPause(){
		super.onPause();
		mapView.onPause();
	}

	@Override public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	@Override public void onLowMemory(){
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override public void onDestroy(){
		super.onDestroy();
		mapView.onDestroy();
	}
}
