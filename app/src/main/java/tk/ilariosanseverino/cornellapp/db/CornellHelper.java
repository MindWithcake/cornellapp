package tk.ilariosanseverino.cornellapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import tk.ilariosanseverino.cornellapp.db.contracts.CampusEntries;
import tk.ilariosanseverino.cornellapp.db.contracts.ContactEntries;
import tk.ilariosanseverino.cornellapp.db.contracts.CourseEntries;
import tk.ilariosanseverino.cornellapp.db.contracts.MemoEntries;
import tk.ilariosanseverino.cornellapp.db.contracts.OccupationEntries;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.CampusDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.ContactDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.CourseDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.OccupationDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

import static tk.ilariosanseverino.cornellapp.db.DefaultValues.*;

/**
 * Helper to access the application database. Created by Ilario Sanseverino on 06/08/15.
 */
public class CornellHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "dbCornell";
	private final static int VERSION = 3;

	private static CornellHelper instance;

	public static CornellHelper getInstance(Context context){
		if(instance == null)
			instance = new CornellHelper(context, DB_NAME, null, VERSION);

		return instance;
	}

	private CornellHelper(Context context, String name, CursorFactory factory, int version){
		super(context, name, factory, version);
	}

	@Override public void onCreate(SQLiteDatabase sqLiteDatabase){
		sqLiteDatabase.beginTransaction();
		try{
			sqLiteDatabase.execSQL(CourseEntries.CREATION);
			sqLiteDatabase.execSQL(CampusEntries.CREATION);
			sqLiteDatabase.execSQL(OccupationEntries.CREATION);
			sqLiteDatabase.execSQL(MemoEntries.CREATION);
			refreshContactTable(sqLiteDatabase);

			for(Course course : COURSES)
				CourseDAOImpl.addCourse(sqLiteDatabase, course);

			for(Campus campus : CAMPUSES)
				CampusDAOImpl.addCampus(sqLiteDatabase, campus);

			for(Occupation time : TIMES)
				OccupationDAOImpl.addOccupation(sqLiteDatabase, time);

			sqLiteDatabase.setTransactionSuccessful();
		}
		finally{
			sqLiteDatabase.endTransaction();
		}
	}

	@Override public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion){
		switch(oldVersion){
		case 1:
			sqLiteDatabase.execSQL(MemoEntries.CREATION);
		case 2:
			sqLiteDatabase.execSQL("DROP TABLE " + ContactEntries.TABLE);
			refreshContactTable(sqLiteDatabase);
		}
	}

	private void refreshContactTable(SQLiteDatabase db){
		db.execSQL(ContactEntries.CREATION);
		for(Contact contact : CONTACTS)
			ContactDAOImpl.addContact(db, contact);
	}

	public static String creationCommand(String table, String id, String... columns){
		StringBuilder b = new StringBuilder(
				"CREATE TABLE " + table + " (" + id + " INTEGER PRIMARY KEY");
		for(int i = 0; i < columns.length; ++i)
			b.append(",").append(columns[i++]).append(columns[i]);
		return b.append(")").toString();
	}
}
