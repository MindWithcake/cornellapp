package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import tk.ilariosanseverino.cornellapp.MapViewFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.CampusDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CampusDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;

/**
 * Fragment to display branches locations. Every main place has a name and a Google Maps view. When
 * you click on the view the Maps application is started.
 * <p/>
 * Created by Ilario Sanseverino on 13/08/15.
 */
public class LocationsFragment extends MapViewFragment
		implements OnMapReadyCallback, SeekBar.OnSeekBarChangeListener {
	private SeekBar seekBar;
	private Marker marker;
	private TextView tooltip;

	private Thread queryThread;
	private CampusDAO dao;
	private List<Campus> places;

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new CampusDAOImpl(activity);
	}

	@Override @Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_locations, container, false);

		mapView = (MapView)root.findViewById(R.id.map_view);
		seekBar = (SeekBar)root.findViewById(R.id.map_slider);
		tooltip = (TextView)root.findViewById(R.id.balloon);

		mapView.onCreate(savedState);
		seekBar.setEnabled(false);

		(queryThread = new QueryThread()).start();

		return root;
	}

	@Override public void onDestroyView(){
		queryThread.interrupt();
		super.onDestroyView();
	}

	@Override public void onMapReady(GoogleMap googleMap){
		seekBar.setEnabled(true);
		Campus place = places.get(seekBar.getProgress());
		LatLng campus = new LatLng(place.latitude, place.longitude);

		if(marker != null)
			marker.remove();
		marker = googleMap.addMarker(new MarkerOptions().position(campus).title(place.name));

		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(campus, 17f), 800, null);
	}

	@Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
		tooltip.setText(places.get(progress).name);
		mapView.getMapAsync(this);
	}

	@Override public void onStartTrackingTouch(SeekBar seekBar){
		tooltip.setVisibility(View.VISIBLE);
	}

	@Override public void onStopTrackingTouch(SeekBar seekBar){
		tooltip.setVisibility(View.GONE);
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.location_title);
	}

	private void setupMap(){
		mapView.getMapAsync(this);

		seekBar.setMax(places.size() - 1);
		seekBar.setOnSeekBarChangeListener(this);

		tooltip.setText(places.get(0).name);
	}

	private class QueryThread extends Thread {
		private Handler handler;

		public QueryThread(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			places = dao.getAllCampus();
			if(places != null && !isInterrupted()){
				handler.post(new Runnable() {
					@Override public void run(){
						setupMap();
					}
				});
			}
		}
	}
}
