package tk.ilariosanseverino.cornellapp.db.daos.implementations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CourseDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;

import static tk.ilariosanseverino.cornellapp.db.contracts.CourseEntries.*;

/**
 * DAO Object to bridge from Course object to the database tables. Created by Ilario Sanseverino on
 * 06/08/15.
 */
public class CourseDAOImpl extends CornellDAO<Course> implements CourseDAO {
	public CourseDAOImpl(Context ctx){
		super(ctx);
	}

	public static long addCourse(SQLiteDatabase db, Course course){
		ContentValues values = new ContentValues();
		values.put(AREA_COL, String.valueOf(course.area));
		values.put(NAME_COL, course.name);
		values.put(ACRONYM_COL, course.acronym);
		values.put(DESCRIPTION_COL, course.description);
		values.put(GRADE_COL, course.grade);
		return db.insert(TABLE, null, values);
	}

	@Override public long addCourse(Course course){
		try(SQLiteDatabase db = helper.getWritableDatabase()){
			return addCourse(db, course);
		}
	}

	@Override public Course getCourse(int id){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryByID(db, TABLE, id)){
			if(!cur.moveToNext())
				return null;
			return makeDAO(cur);
		}
	}

	@Override public List<Course> getCourses(){
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = queryAll(db, TABLE)){
			return makeDAOList(cur);
		}
	}

	@Override public int getID(String courseAcronym){
		String[] whereArg = new String[]{courseAcronym};
		String[] select = new String[]{_ID};
		try(SQLiteDatabase db = helper.getReadableDatabase();
			Cursor cur = db.query(TABLE, select, ACRONYM_COL + "=?", whereArg, null, null, null)){
			if(!cur.moveToNext())
				return -1;
			return cur.getInt(cur.getColumnIndex(_ID));
		}
	}

	@Override Course makeDAO(Cursor cursor){
		String area = cursor.getString(cursor.getColumnIndex(AREA_COL));
		String name = cursor.getString(cursor.getColumnIndex(NAME_COL));
		String acronym = cursor.getString(cursor.getColumnIndex(ACRONYM_COL));
		String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION_COL));
		int grade = cursor.getInt(cursor.getColumnIndex(GRADE_COL));
		return new Course(area, name, acronym, description, grade);
	}
}
