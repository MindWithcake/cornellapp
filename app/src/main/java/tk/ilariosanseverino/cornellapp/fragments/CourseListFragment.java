package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.CourseListAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.CourseDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.CourseDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;

/**
 * List of courses. The courses are in an expandable list, with the area as the main group and the
 * single courses as children items. Created by Ilario Sanseverino on 10/08/2015.
 */
public class CourseListFragment extends CornellFragment implements OnChildClickListener {
	private final static int REQUEST_CODE = 6968748;

	private Thread queryThread;
	private CourseDAO dao;
	private CourseListAdapter adapter = new CourseListAdapter();

	private Course displayed;

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new CourseDAOImpl(activity);
	}

	@Override @Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_course_master, container, false);
		ExpandableListView list = (ExpandableListView)root.findViewById(R.id.course_list);
		list.setGroupIndicator(ContextCompat.getDrawable(root.getContext(),
				R.drawable.course_group_indicator));
		list.setIndicatorBoundsRelative(0, cornellCallbacks.dpToPx(24));
		list.setAdapter(adapter);
		list.setOnChildClickListener(this);

		queryThread = new QueryThread();
		queryThread.start();

		return root;
	}

	@Override public void onDestroyView(){
		super.onDestroyView();
		queryThread.interrupt();
		queryThread = null;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int group, int child, long id){
		displayed = (Course)adapter.getChild(group, child);
		DialogFragment frag = CourseDetailDialogFragment.newInstance(displayed);
		frag.setTargetFragment(this, REQUEST_CODE);
		cornellCallbacks.showDialog(frag);
		return true;
	}

	@Override public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == REQUEST_CODE){
			CornellFragment frag = TimetableDetailCourseFragment.makeInstance(displayed);
			cornellCallbacks.showNewFragment(frag);
		}
	}

	private class QueryThread extends Thread {
		private Handler handler;

		public QueryThread(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			List<Course> courses = dao.getCourses();
			if(courses == null){
				Log.i("CourseList", "No courses received from database");
				return;
			}
			adapter.addCourses(courses);
			if(!isInterrupted()){
				handler.post(new Runnable() {
					@Override public void run(){
						adapter.notifyDataSetChanged();
					}
				});
			}
		}
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.courses_title);
	}
}
