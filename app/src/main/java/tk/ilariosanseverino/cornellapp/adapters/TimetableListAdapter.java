package tk.ilariosanseverino.cornellapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tk.ilariosanseverino.cornellapp.CornellExpandableAdapter;
import tk.ilariosanseverino.cornellapp.CornellExpandableGroup;
import tk.ilariosanseverino.cornellapp.R;

/**
 * Adapter used to display a list of options among which the user can chose which timetable to be
 * shown. Created by Ilario Sanseverino on 15/08/15.
 */
public class TimetableListAdapter extends CornellExpandableAdapter<String, String> {
	@Override
	public View getGroupView(int position, boolean isExpanded, View convertView, ViewGroup parent){
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.view_course_group, parent, false);
		}
		((TextView)convertView).setText(groups.get(position).groupKey);
		return convertView;
	}

	@Override public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
			View convertView, ViewGroup parent){
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.view_course_child, parent, false);
		}
		String child = groups.get(groupPosition).getChildren().get(childPosition);
		((TextView)convertView).setText(child);
		return convertView;
	}

	public void addChild(String groupName, String childName){
		CornellExpandableGroup<String, String> courseGroup = null;
		for(CornellExpandableGroup<String, String> group : groups){
			if(group.groupKey.equalsIgnoreCase(groupName)){
				courseGroup = group;
				break;
			}
		}

		if(courseGroup == null){
			courseGroup = new CornellExpandableGroup<>(groupName);
			groups.add(courseGroup);
		}

		if(!courseGroup.getChildren().contains(childName))
			courseGroup.addChild(childName);
	}
}
