package tk.ilariosanseverino.cornellapp;

import android.widget.BaseExpandableListAdapter;

import java.util.ArrayList;

/**
 * Adapter for {@link android.widget.ExpandableListView} providing most of the boilerplate code.
 * Created by Ilario Sanseverino on 16/08/15.
 */
public abstract class CornellExpandableAdapter<G, C>
		extends BaseExpandableListAdapter {
	/**
	 * Collection containing items representing the groups of the expandable list. It is up to the
	 * implementing subclasses to decide what a group is and hov to display it.
	 */
	protected final ArrayList<CornellExpandableGroup<G, C>> groups;

	public CornellExpandableAdapter(){groups = new ArrayList<>();}

	@Override public int getGroupCount(){
		return groups.size();
	}

	@Override public int getChildrenCount(int i){
		return groups.get(i).getChildren().size();
	}

	@Override public Object getGroup(int i){
		return groups.get(i);
	}

	@Override public Object getChild(int groupPosition, int childPosition){
		return groups.get(groupPosition).getChildren().get(childPosition);
	}

	@Override public long getGroupId(int i){
		return ((long)getGroup(i).hashCode()) << 32;
	}

	@Override public long getChildId(int groupPosition, int childPosition){
		return getGroupId(groupPosition) | getChild(groupPosition, childPosition).hashCode();
	}

	@Override public boolean hasStableIds(){
		return false;
	}

	@Override public boolean isChildSelectable(int groupPosition, int childPosition){
		return true;
	}
}
