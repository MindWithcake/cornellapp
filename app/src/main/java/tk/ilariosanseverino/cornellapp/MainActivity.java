package tk.ilariosanseverino.cornellapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import tk.ilariosanseverino.cornellapp.fragments.AboutFragment;
import tk.ilariosanseverino.cornellapp.fragments.CreditsFragment;
import tk.ilariosanseverino.cornellapp.fragments.WelcomeFragment;

public class MainActivity extends CornellActivity {
	private boolean mDoubleSide;

	@Override public boolean onCreateOptionsMenu(Menu menu){
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}


	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mDoubleSide = findViewById(R.id.master_container) != null;

		if(savedInstanceState == null)
			setupFragments();
	}

	@Override public void showNewFragment(CornellFragment frag){
		FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
		tr.replace(R.id.slave_container, frag).addToBackStack(null).commit();
	}

	@Override public boolean onOptionsItemSelected(MenuItem item){
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		switch(id){
		case R.id.action_settings:
			showDialog(new CreditsFragment());
			return true;
		case R.id.action_memo:
			Intent intent = new Intent(this, MemoActivity.class);
			startActivity(intent);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupFragments(){
		CornellFragment welcome = new WelcomeFragment();
		FragmentTransaction tr = getSupportFragmentManager().beginTransaction();

		if(mDoubleSide)
			tr.add(R.id.master_container, welcome).add(R.id.slave_container, new AboutFragment());
		else
			tr.add(R.id.slave_container, welcome);

		tr.commit();
	}
}
