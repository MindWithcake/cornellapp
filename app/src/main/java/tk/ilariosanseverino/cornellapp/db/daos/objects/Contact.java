package tk.ilariosanseverino.cornellapp.db.daos.objects;

/**
 * Item holding contact information for a Cornell employee. Created by Ilario Sanseverino on
 * 11/08/2015.
 */
public class Contact {
	public final String name;
	public final int photoRes;
	public final String email;
	public final String phoneNum;
	public final String smsNum;

	public Contact(String name, int photoRes, String email, String phoneNum, String smsNum){
		this.name = name;
		this.photoRes = photoRes;
		this.email = email;
		this.phoneNum = phoneNum;
		this.smsNum = smsNum;
	}
}
