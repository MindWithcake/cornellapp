package tk.ilariosanseverino.cornellapp.db.daos.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Item representing an item  for a timetable. This item contains information about the course
 * related to the timetable, the room interested and the date and time.
 * <p/>
 * Created by Ilario Sanseverino on 15/08/15.
 */
public class Occupation implements Parcelable {
	public static final Creator<Occupation> CREATOR = new Creator<Occupation>() {
		@Override public Occupation createFromParcel(Parcel source){
			return new Occupation(source);
		}

		@Override public Occupation[] newArray(int size){
			return new Occupation[size];
		}
	};

	public final String courseArea;
	public final String courseName;
	public final String courseAcronym;

	public final String room;
	public final String campus;

	public final WeekDay weekDay;
	public final int startTime;
	public final int endTime;

	private Occupation(Parcel source){
		this(source.readString(), source.readString(), source.readString(), source.readString(),
				source.readString(), WeekDay.valueOf(source.readString()), source.readInt(),
				source.readInt());
	}

	public Occupation(String courseArea, String courseName, String courseAcronym, String room,
			String campus, WeekDay weekDay, int startTime, int endTime){
		this.courseArea = courseArea;
		this.courseName = courseName;
		this.courseAcronym = courseAcronym;
		this.room = room;
		this.campus = campus;
		this.weekDay = weekDay;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Occupation(Campus campus, Course course, String room, WeekDay day, int start, int end){
		this(course.area, course.name, course.acronym, room, campus.name, day, start, end);
	}

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeString(courseArea);
		dest.writeString(courseName);
		dest.writeString(courseAcronym);
		dest.writeString(room);
		dest.writeString(campus);
		dest.writeString(weekDay.name());
		dest.writeInt(startTime);
		dest.writeInt(endTime);
	}

	public enum WeekDay {
		Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
	}
}
