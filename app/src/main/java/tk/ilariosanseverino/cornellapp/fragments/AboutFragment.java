package tk.ilariosanseverino.cornellapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;

/**
 * Fragment for the "About" page. It's just a WebView with content from the web page. Created by
 * Ilario Sanseverino on 15/08/15.
 */
public class AboutFragment extends CornellFragment {
	@Override @Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_about, container, false);

		WebView webView = (WebView)root.findViewById(R.id.html_view);
		webView.loadData(getText(R.string.about_content).toString(), "text/html", "UTF8");

		return root;
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.about_title);
	}
}
