package tk.ilariosanseverino.cornellapp.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import static android.view.View.MeasureSpec.*;

/**
 * {@link TextView} guaranteed to have equal apparent width and height. The size of the sides are
 * chosen as the biggest possible according to the given constraints. If a rectangular area must be
 * filled then white margins are added to keep the even proportion. Created by Ilario Sanseverino on
 * 02/08/15.
 */
public class SquareTextView extends TextView {
	private int marginHorizontal, marginVertical;

	public SquareTextView(Context context){
		this(context, null);
		setGravity(Gravity.CENTER);
	}

	public SquareTextView(Context context, AttributeSet attrs){
		this(context, attrs, 0);
	}

	public SquareTextView(Context context, AttributeSet attrs, int defStyleAttr){
		super(context, attrs, defStyleAttr);
	}

	@Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);

		if(widthMode == UNSPECIFIED && heightMode == UNSPECIFIED)
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		else{
			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = MeasureSpec.getSize(heightMeasureSpec);
			int size;

			if(widthMode != UNSPECIFIED)
				size = heightMode == UNSPECIFIED? width : Math.min(width, height);
			else
				size = height;

			marginHorizontal = Math.abs(width - size);
			marginVertical = Math.abs(height - size);
			super.onMeasure(makeMeasureSpec(size, EXACTLY), makeMeasureSpec(size, EXACTLY));
		}
	}

	@Override protected void onLayout(boolean changed, int left, int top, int right, int bottom){
		int h = marginVertical / 2, w = marginHorizontal / 2;
		super.onLayout(changed, left + w, top + h, right + w, bottom + h);
	}

	@Override protected void onSizeChanged(int w, int h, int oldW, int oldH){
		int shortestSide = Math.min(w, h);
		super.onSizeChanged(shortestSide, shortestSide, oldW, oldH);
	}
}
