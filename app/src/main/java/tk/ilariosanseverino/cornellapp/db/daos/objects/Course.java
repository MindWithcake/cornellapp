package tk.ilariosanseverino.cornellapp.db.daos.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Item for storing information about a single course.
 * Created by Ilario Sanseverino on 06/08/15.
 */
public class Course implements Parcelable {
	public static final Creator<Course> CREATOR = new Creator<Course>() {
		@Override public Course createFromParcel(Parcel in){
			return new Course(in);
		}

		@Override public Course[] newArray(int size){
			return new Course[size];
		}
	};
	public final String area;
	public final String name;
	public final String acronym;
	public final String description;
	public final int grade;

	public Course(String area, String name, String acronym, String description, int grade){
		this.area = area;
		this.name = name;
		this.acronym = acronym;
		this.description = description;
		this.grade = grade;
	}

	private Course(Parcel in){
		this(in.readString(), in.readString(), in.readString(), in.readString(), in.readInt());
	}

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeString(area);
		dest.writeString(name);
		dest.writeString(acronym);
		dest.writeString(description);
		dest.writeInt(grade);
	}
}
