package tk.ilariosanseverino.cornellapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Adapter for the timetable detail list showing occupation by course Created by Ilario Sanseverino
 * on 17/08/15.
 */
public class ByCourseAdapter extends ArrayAdapter<Occupation> {
	public ByCourseAdapter(Context context){
		super(context, 0);
	}

	@Override public View getView(int position, View convert, ViewGroup parent){
		ViewHolder holder;
		if(convert == null){
			holder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convert = inflater.inflate(R.layout.view_timetable_course_detail_item, parent, false);
			holder.day = (TextView)convert.findViewById(R.id.day_label);
			holder.time = (TextView)convert.findViewById(R.id.time_label);
			holder.room = (TextView)convert.findViewById(R.id.room_label);
			convert.setTag(holder);
		}
		else
			holder = (ViewHolder)convert.getTag();

		int bgRes;
		switch(position % 3){
		case 0:
			bgRes = R.color.list_even;
			break;
		case 1:
			bgRes = R.color.list_odd;
			break;
		default:
			bgRes = R.color.list_third;
			break;
		}
		convert.setBackgroundColor(getContext().getResources().getColor(bgRes));

		Occupation occupation = getItem(position);
		setContent(occupation, holder);

		return convert;
	}

	protected void setContent(Occupation occupation, ViewHolder holder){
		holder.day.setText(occupation.weekDay.name());
		holder.room.setText(occupation.campus + "\n" + occupation.room);
		holder.time.setText(timeString(occupation.startTime, occupation.endTime));
	}

	private String timeString(int time){
		int min = time % 100;
		int hour = (time / 100) % 100;
		return String.format("%02d:%02d", hour, min);
	}

	protected String timeString(int start, int stop){
		return timeString(start) + " - " + timeString(stop);
	}

	protected static class ViewHolder {
		protected TextView day;
		protected TextView time;
		protected TextView room;
	}
}
