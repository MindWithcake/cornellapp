package tk.ilariosanseverino.cornellapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;

/**
 * This is the first fragment shown when the application is opened. It simply contains the choices
 * to the various available pages. On larger devices it will be on the left, while the chosen page
 * is shown on the right part of the screen. Created by Ilario Sanseverino on 02/08/15.
 */
public class WelcomeFragment extends CornellFragment {

	@Override @Nullable
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_welcome, container, false);

		for(final WelcomeButton button : WelcomeButton.values()){
			root.findViewById(button.ID).setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View v){
					cornellCallbacks.showNewFragment(button.frag);
				}
			});
		}

		return root;
	}

	private enum WelcomeButton {
		ABOUT(R.id.info_button, new AboutFragment()),
		COURSES(R.id.courses_button, new CourseListFragment()),
		TIMETABLES(R.id.timetables_button, new TimetablesFragment()),
		LOCATIONS(R.id.locations_button, new LocationsFragment()),
		CONTACTS(R.id.contacts_button, new ContactsFragment());

		private final int ID;
		private final CornellFragment frag;

		WelcomeButton(int id, CornellFragment frag){
			ID = id;
			this.frag = frag;
		}
	}
}
