package tk.ilariosanseverino.cornellapp.db.daos.interfaces;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 *
 * Created by Ilario Sanseverino on 16/08/15.
 */
public interface OccupationDAO {
	long addOccupation(Occupation occupation);

	Occupation getOccupation(int id);

	List<Occupation> getOccupations();

	List<Occupation> getCourseOccupations(Course course);

	List<Occupation> getRoomOccupations(String room);

	List<Occupation> getDailyOccupations(Occupation.WeekDay day);
}
