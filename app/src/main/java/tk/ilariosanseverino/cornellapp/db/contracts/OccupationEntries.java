package tk.ilariosanseverino.cornellapp.db.contracts;

import android.provider.BaseColumns;

import tk.ilariosanseverino.cornellapp.db.CornellHelper;

/**
 * Contract for the Occupation table. Created by Ilario Sanseverino on 17/08/15.
 */
public abstract class OccupationEntries implements BaseColumns {
	public static final String TABLE = "tbOccupation";

	public static final String COURSE_COL = "OccupationCourse";
	public static final String COURSE_TYPE = " INTEGER REFERENCES " + CourseEntries.TABLE +
			" ON UPDATE CASCADE";

	public static final String ROOM_COL = "OccupationRoom";
	public static final String ROOM_TYPE = " TEXT";

	public static final String CAMPUS_COL = "OccupationCampus";
	public static final String CAMPUS_TYPE = " INTEGER REFERENCES " + CampusEntries.TABLE +
			" ON UPDATE CASCADE";

	public static final String DAY_COL = "OccupationDay";
	public static final String DAY_TYPE = " TEXT";

	public static final String START_COL = "OccupationStartTime";
	public static final String START_TYPE = " INTEGER";

	public static final String END_COL = "OccupationEndTime";
	public static final String END_TYPE = " INTEGER";

	public static final String CREATION = CornellHelper.creationCommand(TABLE, _ID, COURSE_COL,
			COURSE_TYPE, ROOM_COL, ROOM_TYPE, CAMPUS_COL, CAMPUS_TYPE, DAY_COL, DAY_TYPE,
			START_COL,
			START_TYPE, END_COL, END_TYPE);

	private OccupationEntries(){}
}
