package tk.ilariosanseverino.cornellapp.db;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Campus;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Contact;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

import static tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation.WeekDay.*;

/**
 * Static values to be added to the database.
 *
 * Created by Ilario Sanseverino on 18/08/15.
 */
public class DefaultValues {

	public final static List<Contact> CONTACTS = new ArrayList<>();

	static{
		CONTACTS.add(new Contact("David Xu", R.drawable.david_photo, "david@cornell.ac.nz",
				"+64 9 915 3506", ""));
		CONTACTS.add(new Contact("Shaik F Ahmed", R.drawable.shaik_photo, "shaik@cornell.ac.nz",
				"+64 9 915 3507", ""));
		CONTACTS.add(new Contact("Regina Lee", R.drawable.regina_photo, "regina@cornell.ac.nz",
				"+64 9 367 3504", ""));
		CONTACTS.add(new Contact("Bien P. Tabo", R.drawable.bien_photo, "bien@cornell.ac.nz",
				"+64 9 367 3526", ""));
		CONTACTS.add(new Contact("Norikazu Ozaki", R.drawable.nori_photo, "nori@cornell.ac.nz",
				"+64-9-367-1010", ""));
		CONTACTS.add(new Contact("I.S.A Rahman", R.drawable.rahman_photo, "rahman@cornell.ac.nz",
				"+64 9 367 3555", ""));
		CONTACTS.add(new Contact("Jenny Brunton", R.drawable.jenny_photo,
				"jenny.brunton@cornell.ac.nz", "+64-9-367-1010", ""));
		CONTACTS.add(new Contact("Leon Luo", R.drawable.leon_photo, "liang@cornell.ac.nz",
				"+64-9-367-1010", ""));
	}

	public final static List<Course> COURSES = new ArrayList<>();

	static{
		COURSES.add(new Course("IT",
				"Diploma in Information Technology (Networking Technology and Administration)",
				"DITL5", "The Diploma gives students an excellent foundation for a network and " +
				"server administration career. Graduates will be prepared to assume jobs " +
						"for supporting modern computing networks and administering network " +
						"server systems. It offers the opportunity to become Certified IT " +
						"Professional (MCITP) and acquire some CompTIA certifications. " +
						"Certifications help employers decide whether applicants have the " +
						"required technical skills.", 5));
		COURSES.add(new Course("IT",
				"Diploma in Information Technology (Networking Technology Management)", "DITL7",
				"Graduates will gain theoretical and practical skills to build, optimise and" +
						" manage complex commercial networks and network server systems. The " +
						"Diploma offers an opportunity to become Cisco certified and prepares" +
						" you for Cisco Certified Network Associate (CCNA) and Cisco Certified" +
						" Network Professional (CCNP). This diploma provides opportunity for " +
						"those who are already in the industry to gain academic recognition " +
						"for their skills.", 7));
		COURSES.add(new Course("IT", "Diploma in Computer Software Development", "CSDL7",
				"Aim of the course is to teach students software development through a " +
						"practical approach. Particular emphasis is given to the most " +
						"important features real IT companies are looking for:\n - Object " +
						"Oriented Programming\n - Web Development\n - Mobile Software " +
						"Development", 7));
		COURSES.add(new Course("Business", "National Diploma in Business", "BL5",
				"The National Diploma programme Levels 5provides a broad set of business " +
						"skills practiced in the industry. After graduation you will be able" +
						" to:\n - Work in entry level business jobs.\n - Progress into " +
						"further specializations in management, marketing, human resources." +
						"\n - Progress to Year 2 (Level 6) of this program or equivalent " +
						"programs that explore more business skills and knowledge areas.", 5));
		COURSES.add(new Course("Business", "National Diploma in Business", "BL6",
				"A further year of study builds upon the knowledge and skills students have " +
						"developed in the Level 5 course. The focus is on management, " +
						"marketing and accounting and we will give you the project management" +
						" skills you will need to play a part in the development of " +
						"large-scale business projects.", 6));
		COURSES.add(new Course("Business", "Cornell Diploma in Business", "NZDB",
				"The business environment is changing rapidly — and constantly. Do you " +
						"understand the implications of the free trade agreement with China," +
						" or why the collapse of finance companies triggered the global " +
						"recession? Why are house prices falling while the price of oil " +
						"continues to rise? Study business level 7 and discover the answers " +
						"to these questions and more!", 6));
		COURSES.add(new Course("Cookery", "Diploma in Professional Cookery", "COOK1",
				"Cornell offers an in depth Diploma course taught over a one year period. With" +
						" a thorough training in the skills of professional cookery, you will " +
						"learn everything required to prepare classical European dishes " +
						"through to the creative innovations of modern cuisine. Our " +
						"professional trainers, coupled with modern training kitchens, along " +
						"with our years of experience in educational excellence make us a " +
						"first choice training establishment in the careers of future " +
						"international chefs.", 5));
		COURSES.add(new Course("Cookery", "Diploma in International Culinary Arts", "COOK2",
				"Cornell offers an in depth Diploma course taught over a two year period: four" +
						" modules in the first year lead to the Cornell Certificate in " +
						"Professional Cookery level 4; the second year is devoted to the " +
						"Diploma in International Culinary Arts.\nWith a thorough training in" +
						" the skills of professional cookery, you will learn everything " +
						"required to prepare classical European dishes through to the creative" +
						" innovations of modern cuisine. Our professional trainers, coupled " +
						"with modern training kitchens, along with our years of experience in " +
						"educational excellence make us a first choice training establishment " +
						"for the future careers of international chefs.", 5));
	}

	public final static List<Campus> CAMPUSES = new ArrayList<>();

	static{
		CAMPUSES.add(new Campus("Main", -36.85116, 174.760661));
		CAMPUSES.add(new Campus("Queen", -36.852747, 174.764034));
		CAMPUSES.add(new Campus("PBRS", -36.851312, 174.759839));
		CAMPUSES.add(new Campus("Tauranga", -37.684533, 176.168772));
		CAMPUSES.add(new Campus("Panmure", -36.899835, 174.855111));
		CAMPUSES.add(new Campus("Wilkinson", -43.526774, 172.608575));
	}

	public final static List<Occupation> TIMES = new ArrayList<>();

	static{
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Monday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Monday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "205", Monday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "205", Monday, 1540, 1740));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Wednesday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Wednesday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Thursday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "203", Thursday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "202", Thursday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(0), "205", Thursday, 1540, 1740));

		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "201", Tuesday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "201", Tuesday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "103", Tuesday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "105", Tuesday, 1540, 1740));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "103", Wednesday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "104", Wednesday, 1540, 1740));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "201", Friday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "201", Friday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "103", Friday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(1), "105", Friday, 1540, 1740));

		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(2), "305", Tuesday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(2), "203", Tuesday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(2), "201", Tuesday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(2), "201", Tuesday, 1540, 1740));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(2), "202", Thursday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(2), "104", Thursday, 1100, 1300));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(2), "203", Thursday, 1340, 1540));
		TIMES.add(new Occupation(CAMPUSES.get(0), COURSES.get(2), "203", Thursday, 1540, 1740));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(2), "105", Friday, 900, 1100));
		TIMES.add(new Occupation(CAMPUSES.get(2), COURSES.get(2), "105", Friday, 1100, 1300));
	}
}
