package tk.ilariosanseverino.cornellapp.fragments.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import java.util.Calendar;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation.WeekDay;
import tk.ilariosanseverino.cornellapp.fragments.TimetableDetailDateFragment;

import static tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation.WeekDay.*;

/**
 * Fragment to let the user chose a date and see timetables for that day.
 * Created by Ilario Sanseverino on 15/08/15.
 */
public class DateTimetableFragment extends CornellFragment implements View.OnClickListener {
	private CalendarView calendar;

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_timetable_date_pick, container, false);

		calendar = (CalendarView)root.findViewById(R.id.calendarView);
		View confirm = root.findViewById(R.id.go_button);

		confirm.setOnClickListener(this);

		return root;
	}

	@Override public void onClick(View v){
		WeekDay day = getFromDate(calendar.getDate());
		cornellCallbacks.showNewFragment(TimetableDetailDateFragment.makeInstance(day));
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.time_title);
	}

	private WeekDay getFromDate(long date){
		Calendar javaCal = Calendar.getInstance();
		javaCal.setTimeInMillis(date);
		int day = javaCal.get(Calendar.DAY_OF_WEEK);
		switch(day){
		case Calendar.MONDAY:
			return Monday;
		case Calendar.TUESDAY:
			return Tuesday;
		case Calendar.WEDNESDAY:
			return Wednesday;
		case Calendar.THURSDAY:
			return Thursday;
		case Calendar.FRIDAY:
			return Friday;
		case Calendar.SATURDAY:
			return Saturday;
		case Calendar.SUNDAY:
			return Sunday;
		default:
			throw new IllegalArgumentException("This should never happen...");
		}
	}
}
