package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.TimetablePagerAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.implementations.OccupationDAOImpl;
import tk.ilariosanseverino.cornellapp.db.daos.interfaces.OccupationDAO;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Fragment for the selection of the type of timetable to show. Based on the type different option
 * are given to chose the actual timetable to display.
 * <p/>
 * Created by Ilario Sanseverino on 15/08/15.
 */
public class TimetablesFragment extends CornellFragment {
	private ViewPager pager;
	private TabLayout tabs;

	private Thread queryThread;
	private OccupationDAO dao;

	@Override public void onAttach(Activity activity){
		super.onAttach(activity);
		dao = new OccupationDAOImpl(activity);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_timetables, container, false);

		pager = (ViewPager)root.findViewById(R.id.timetables_pager);
		tabs = (TabLayout)root.findViewById(R.id.sliding_tabs);

		(queryThread = new QueryThread()).start();

		return root;
	}

	@Override public void onDestroyView(){
		queryThread.interrupt();
		super.onDestroyView();
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.time_title);
	}

	private class QueryThread extends Thread {
		private Handler handler;

		public QueryThread(){
			handler = new Handler(Looper.getMainLooper());
		}

		@Override public void run(){
			final List<Occupation> times = dao.getOccupations();
			if(times != null && !isInterrupted()){
				handler.post(new Runnable() {
					@Override public void run(){
						FragmentManager fm = getChildFragmentManager();
						pager.setAdapter(new TimetablePagerAdapter(fm, new ArrayList<>(times)));
						tabs.post(new Runnable() {
							@Override public void run(){
								tabs.setupWithViewPager(pager);
							}
						});
					}
				});
			}
		}
	}
}
