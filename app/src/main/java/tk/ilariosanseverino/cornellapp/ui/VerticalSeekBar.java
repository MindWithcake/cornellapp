package tk.ilariosanseverino.cornellapp.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

/**
 * Seek Bar vertically oriented. This code has been modified starting from various sources:<br/> <a
 * href="https://github.com/AndroSelva/Vertical-SeekBar-Android">AndroSelva Vertical SeekBar</a>.
 * <br/><a href="https://hackskrieg.wordpress.com/2012/04/20/working-vertical-seekbar-for-android/">
 * Hackskrieg Blog</a>.
 * <p/>
 * Created by Ilario Sanseverino on 20/08/2015.
 */
public class VerticalSeekBar extends SeekBar {
	private OnSeekBarChangeListener onChangeListener;
	private int lastProgress = 0;

	public VerticalSeekBar(Context context){
		super(context);
	}

	public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
	}

	public VerticalSeekBar(Context context, AttributeSet attrs){
		super(context, attrs);
	}

	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		super.onSizeChanged(h, w, oldh, oldw);
	}

	@Override protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		//noinspection SuspiciousNameCombination
		super.onMeasure(heightMeasureSpec, widthMeasureSpec);
		setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
	}

	@Override protected void onDraw(@NonNull Canvas c){
		c.rotate(90);
		c.translate(0, -getWidth());

		super.onDraw(c);
	}

	@Override public boolean onTouchEvent(@NonNull MotionEvent event){
		if(!isEnabled()){
			return false;
		}

		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			onChangeListener.onStartTrackingTouch(this);
			setPressed(true);
			setSelected(true);
			break;
		case MotionEvent.ACTION_MOVE:
			super.onTouchEvent(event);
			int progress = Math.round(getMax() * event.getY() / getHeight());

			// Ensure progress stays within boundaries
			progress = Math.max(0, Math.min(getMax(), progress));
			setProgress(progress);

			if(progress != lastProgress){
				// Only enact listener if the progress has actually changed
				lastProgress = progress;
				onChangeListener.onProgressChanged(this, progress, true);
			}

			onSizeChanged(getWidth(), getHeight(), 0, 0);
			setPressed(true);
			setSelected(true);
			break;
		case MotionEvent.ACTION_UP:
			onChangeListener.onStopTrackingTouch(this);
			setPressed(false);
			setSelected(false);
			break;
		case MotionEvent.ACTION_CANCEL:
			super.onTouchEvent(event);
			setPressed(false);
			setSelected(false);
			break;
		}
		return true;
	}

	@Override public void setOnSeekBarChangeListener(OnSeekBarChangeListener onChangeListener){
		this.onChangeListener = onChangeListener;
	}
}