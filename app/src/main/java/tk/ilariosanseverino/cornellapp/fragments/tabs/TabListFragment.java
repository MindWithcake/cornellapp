package tk.ilariosanseverino.cornellapp.fragments.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import java.util.ArrayList;
import java.util.List;

import tk.ilariosanseverino.cornellapp.CornellFragment;
import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.adapters.TimetableListAdapter;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Abstract Fragment used to create timetable choice list. Implements the common code for
 * saving/retrieving time list. Created by Ilario Sanseverino on 16/08/15.
 */
public abstract class TabListFragment extends CornellFragment implements OnChildClickListener {
	private final static String TAG_TIMES = "TabListFragment.TAG_TIMES";

	protected TimetableListAdapter adapter;
	private ArrayList<Occupation> occupations;

	protected void addArgument(ArrayList<Occupation> times){
		Bundle arg = getArguments();
		if(arg == null){
			arg = new Bundle();
			setArguments(arg);
		}

		arg.putParcelableArrayList(TAG_TIMES, times);
	}

	@Override public void onCreate(@Nullable Bundle savedState){
		super.onCreate(savedState);

		if(savedState == null)
			savedState = getArguments();

		occupations = savedState.getParcelableArrayList(TAG_TIMES);
		if(occupations != null)
			adapter = getAdapter(occupations);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_course_master, container, false);

		ExpandableListView list = (ExpandableListView)root.findViewById(R.id.course_list);
		list.setAdapter(adapter);
		list.setOnChildClickListener(this);

		return root;
	}

	@Override public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList(TAG_TIMES, occupations);
	}

	@Override protected CharSequence getTitle(){
		return getString(R.string.time_title);
	}

	protected abstract TimetableListAdapter getAdapter(List<Occupation> times);
}
