package tk.ilariosanseverino.cornellapp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;
import tk.ilariosanseverino.cornellapp.ui.DateView;

/**
 * Holder for the Memo Recycler.
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class MemoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private DateView dateView;
	private TextView nameView;
	private TextView notesView;
	private Memo memo;

	private OnMemoCardClick mListener;

	public MemoHolder(View itemView, OnMemoCardClick listener){
		super(itemView);
		dateView = (DateView)itemView.findViewById(R.id.date_icon);
		nameView = (TextView)itemView.findViewById(R.id.event_name);
		notesView = (TextView)itemView.findViewById(R.id.event_notes);
		mListener = listener;
		itemView.setOnClickListener(this);
	}

	public void bindMemo(Memo memo){
		dateView.setDate(memo.date);
		nameView.setText(memo.name);
		notesView.setText(memo.notes);
		this.memo = memo;
	}

	@Override public void onClick(View v){
		if(mListener != null)
			mListener.onMemoCardClick(memo);
	}

	public interface OnMemoCardClick {
		void onMemoCardClick(Memo memo);
	}
}
