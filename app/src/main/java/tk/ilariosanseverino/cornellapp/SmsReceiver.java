package tk.ilariosanseverino.cornellapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * BroadcastReceiver to receive SMS. Created by Ilario Sanseverino on 18/08/15.
 */
public class SmsReceiver extends BroadcastReceiver {
	private final static String TAG = "CornellApp";

	@Override public void onReceive(Context context, Intent intent){
		Log.i(TAG, "Sms received");

		new Thread(new RunnableOne()).start();
		new Thread(new RunnableTwo()).start();
		new Thread(new RunnableThree()).start();
		new Thread(new RunnableFour()).start();
	}

	private static class RunnableOne implements Runnable {
		@Override public void run(){
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException e){
				Log.e(TAG, "Thread interrupted unexpectedly");
			}
		}
	}

	private static class RunnableTwo implements Runnable {
		@Override public void run(){
			try{
				Thread.sleep((long)(Math.random() * 5000));
				Log.i(TAG, "I slept very well");
			}
			catch(InterruptedException e){
				Log.d(TAG, "Someone ruined my sleep :(");
			}
		}
	}

	private static class RunnableThree implements Runnable {
		@Override public void run(){
			Log.v(TAG, "I'm running from a thread ^^");
		}
	}

	private static class RunnableFour implements Runnable {
		@Override public void run(){
			Log.v(TAG, "Me too :p");
		}
	}
}
