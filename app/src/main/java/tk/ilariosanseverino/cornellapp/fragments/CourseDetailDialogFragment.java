package tk.ilariosanseverino.cornellapp.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tk.ilariosanseverino.cornellapp.R;
import tk.ilariosanseverino.cornellapp.db.daos.objects.Course;

/**
 * DialogFragment to show detailed information about a course. Created by Ilario Sanseverino on
 * 17/08/15.
 */
public class CourseDetailDialogFragment extends DialogFragment implements View.OnClickListener {
	private final static String COURSE_TAG = "CourseDetailDialogFragment.COURSE_TAG";

	private Course mCourse;

	private boolean buttonPressed = false;

	public static DialogFragment newInstance(Course course){
		Bundle arg = new Bundle();
		arg.putParcelable(COURSE_TAG, course);

		DialogFragment frag = new CourseDetailDialogFragment();
		frag.setArguments(arg);
		return frag;
	}

	@Override public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mCourse = getArguments().getParcelable(COURSE_TAG);
	}

	@Nullable @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState){
		View root = inflater.inflate(R.layout.fragment_dialog_course_detail, container, false);

		TextView description = (TextView)root.findViewById(R.id.course_description);
		Button button = (Button)root.findViewById(R.id.show_time_button);

		description.setText(mCourse.description);
		button.setOnClickListener(this);

		return root;
	}

	@NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState){
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		TextView tv = (TextView)dialog.findViewById(android.R.id.title);
		tv.setText(mCourse.name);
		tv.setEllipsize(TextUtils.TruncateAt.MIDDLE);
		tv.setMaxLines(1);
		tv.setHorizontallyScrolling(true);
		return dialog;
	}

	@Override public void onClick(View v){
		buttonPressed = true;
		dismiss();
	}

	@Override public void onDismiss(DialogInterface dialog){
		super.onDismiss(dialog);
		if(buttonPressed)
			getTargetFragment().onActivityResult(getTargetRequestCode(), 0, null);
	}
}
