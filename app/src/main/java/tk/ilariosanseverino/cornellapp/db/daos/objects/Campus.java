package tk.ilariosanseverino.cornellapp.db.daos.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Object representing a Cornell Campus.
 * Created by Ilario Sanseverino on 16/08/15.
 */
public class Campus implements Parcelable {
	public static final Creator<Campus> CREATOR = new Creator<Campus>() {
		@Override public Campus createFromParcel(Parcel in){
			return new Campus(in);
		}

		@Override public Campus[] newArray(int size){
			return new Campus[size];
		}
	};
	public final String name;
	public final String description;
	public final double latitude;
	public final double longitude;

	public Campus(String name, String description, double latitude, double longitude){
		this.name = name;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Campus(String name, double latitude, double longitude){
		this(name, null, latitude, longitude);
	}

	private Campus(Parcel in){
		this(in.readString(), in.readString(), in.readDouble(), in.readDouble());
	}

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeString(name);
		dest.writeString(description);
		dest.writeDouble(latitude);
		dest.writeDouble(longitude);
	}
}
