package tk.ilariosanseverino.cornellapp.db.daos.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Object containing Memo information.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public class Memo implements Parcelable {
	public final String name;
	public final String notes;
	public final long date;
	public long id;

	public Memo(String name, String notes, long date, long id){
		this.name = name;
		this.notes = notes;
		this.date = date;
		this.id = id;
	}

	private Memo(Parcel in){
		this(in.readString(), in.readString(), in.readLong(), in.readLong());
	}

	public static final Creator<Memo> CREATOR = new Creator<Memo>() {
		@Override public Memo createFromParcel(Parcel in){
			return new Memo(in);
		}

		@Override public Memo[] newArray(int size){
			return new Memo[size];
		}
	};

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeString(name);
		dest.writeString(notes);
		dest.writeLong(date);
		dest.writeLong(id);
	}
}
