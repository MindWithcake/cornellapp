package tk.ilariosanseverino.cornellapp.db.daos.interfaces;

import java.util.List;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Memo;

/**
 * DAO interface to access the Memo table.
 * <p/>
 * Created by Ilario Sanseverino on 21/08/15.
 */
public interface MemoDAO {
	long addMemo(Memo memo);

	Memo getMemo(int id);

	List<Memo> getAllMemos();

	int getID(Memo memo);

	void deleteMemo(long id);
}
