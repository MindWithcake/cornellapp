package tk.ilariosanseverino.cornellapp.adapters;

import android.content.Context;

import tk.ilariosanseverino.cornellapp.db.daos.objects.Occupation;

/**
 * Adapter to show timetables for a single day.
 * Created by Ilario Sanseverino on 19/08/15.
 */
public class ByDateAdapter extends ByCourseAdapter {
	public ByDateAdapter(Context context){
		super(context);
	}

	@Override protected void setContent(Occupation occupation, ViewHolder holder){
		holder.room.setText(occupation.campus + "\n" + occupation.room);
		holder.day.setText(occupation.courseAcronym);
		holder.time.setText(timeString(occupation.startTime, occupation.endTime));
	}
}
